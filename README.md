# 介绍
AdGuardHome自用黑白名单补充规则  
* 黑名单为补充下面几个规则的漏网之鱼
* 白名单为不能影响常用网站的使用
* 自用规则发现广告或误拦即添加！
* 补充数量: 139条
* 最近更新: 2023年09月13日18:45:12
# 黑名单地址
* 直链:  
`https://raw.githubusercontent.com/mphin/AdGuardHome_rules/main/777_Blacklist.txt`  
* CDN:  
`https://cdn.jsdelivr.net/gh/mphin/AdGuardHome_rules@main/777_Blacklist.txt`  
# 白名单地址 
* 直链:  
`https://raw.githubusercontent.com/mphin/AdGuardHome_rules/main/777_Allowlist.txt`  
* CDN:  
`https://cdn.jsdelivr.net/gh/mphin/AdGuardHome_rules@main/777_Allowlist.txt`  

# 使用以下规则进行补充  
黑名单：   
* AdAway  
`https://adaway.org/hosts.txt`  
* 乘风视频广告  
`https://cdn.jsdelivr.net/gh/xinggsf/Adblock-Plus-Rule@master/mv.txt`
* 乘风广告  
`https://cdn.jsdelivr.net/gh/xinggsf/Adblock-Plus-Rule@master/rule.txt`  
* adgk手机去广告规则  
`https://fastly.jsdelivr.net/gh/banbendalao/ADgk@master/ADgk.txt`  
* anti-AD  
`https://anti-ad.net/easylist.txt`  
* EasyList针对国内的补充规则  
`https://easylist-downloads.adblockplus.org/easylistchina.txt`  
* EasyList去除国际  
`https://easylist-downloads.adblockplus.org/easylist.txt`  

白名单：  
* anti白名单  
`https://fastly.jsdelivr.net/gh/privacy-protection-tools/dead-horse@master/anti-ad-white-list.txt`  


        
本仓库创建于2023年7月2日
